#! /usr/bin/env python

import pygame
from pygame.locals import *
from Constants import *
from Images import *

class COINS(pygame.sprite.Sprite):

    def __init__(self,X,Y,W,H):

        super(COINS,self).__init__()
        
        #self.image=pygame.transform.scale(pygame.image.load(Image_Location),(W,H))
        self.image=pygame.transform.scale(coin_images[0],(W,H))
        self.rect=self.image.get_rect()
        self.rect.x=X
        self.rect.y=Y

    def rotate(self,index):

        self.image=pygame.transform.scale(coin_images[index],(20,20))
