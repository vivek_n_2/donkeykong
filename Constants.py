import pygame

# Colors

aqua      =  (  0  , 255 , 255 )
black     =  (  0  ,  0  ,  0  )
blue      =  (  0  ,  0  , 255 )
fuchsia   =  ( 255 ,  0  , 255 )
gray      =  ( 128 , 128 , 128 )
green     =  (  0  , 128 ,  0  )
lime      =  (  0  , 255 ,  0  )
maroon    =  ( 128 ,  0  ,  0  )
navyblue  =  (  0  ,  0  , 128 )
olive     =  ( 128 , 128 ,  0  )
purple    =  ( 128 ,  0  , 128 )
red       =  ( 150 ,  0  ,  0  )
silver    =  ( 192 , 192 , 192 )
teal      =  (  0  , 128 , 128 )
white     =  ( 255 , 255 , 255 )
yellow    =  ( 255 , 255 ,  0  )

# Screen parameters

screen_width    =  1300
screen_height   =  640
screen=pygame.display.set_mode((screen_width,screen_height))

floor_height    =  20
free_space      =  60
Padding_top     =  60
Padding_bot     =  40
Queen_space     =  20
Hero_Height     =  20
Hero_xmove      =  5
Hero_ymove      =  5
path_len        =  1260 #screen_width-2*(floor_height)
Total_levels    =  6
block_size      =  20

#block_size      =  screen_height//30

top     =   Padding_top
right   =   screen_width - floor_height
left    =   floor_height
bottom  =   screen_height - Padding_bot - floor_height
diff    =   free_space+floor_height

border_attributes=[]

for i in range(screen_width//block_size):
    border_attributes.append((i*block_size,top))
    border_attributes.append((i*block_size,bottom))

for i in range(1,((bottom-top)//block_size)):
    border_attributes.append((0,top+i*block_size))
    border_attributes.append((right,top+i*block_size))

jumps=[-9,-7,-7,-5,-5,-3,-2,-2,2,2,3,5,5,7,7,9]
