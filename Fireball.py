#! /usr/bin/env python

import pygame
from pygame.locals import *
from Constants import *
from Generator import *
from Images import *

class BALL(pygame.sprite.Sprite):

    def __init__(self,X,Y,W,H,screen):

        super(BALL,self).__init__()
        
        self.image=pygame.transform.scale(fire_images[0],(W,H))
        self.rect=self.image.get_rect()
        self.rect.x=X
        self.rect.y=Y
        self.image.set_colorkey(black)

        self.xmove=2
        self.ymove=2
        self.fall=False

    def gravity(self):

        for obj in wall_group:
            
            if obj.rect.top==self.rect.bottom:

                if self.rect.left>=obj.rect.left and self.rect.left<obj.rect.right:

                    return False
                
                if self.rect.right>obj.rect.left and self.rect.right<=obj.rect.right:

                    return False

        return True

    def onladder(self):
        
        for obj in ladder_group:
            
            if obj.rect.top==self.rect.bottom:

                if self.rect.left>=obj.rect.left and self.rect.left<obj.rect.right:

                    return True
                
                if self.rect.right>obj.rect.left and self.rect.right<=obj.rect.right:

                    return True

        return False

    def rotate(self,index):
        
        self.image=pygame.transform.scale(fire_images[index],(20,20))


    def movement(self):
        
        for obj in wall_group:
            
            if obj.rect.bottom==self.rect.bottom:
                
                if obj.rect.right==self.rect.left and self.xmove<0:

                    self.xmove*=-1
                
                if obj.rect.left==self.rect.right and self.xmove>0:

                    self.xmove*=-1

        if self.fall and not self.gravity():

            self.fall=False

            rand=random.randrange(2)

            if rand==1:

                self.xmove*=-1

        if not self.fall and self.gravity():

            if not self.onladder():

                self.fall=True
            else:

                rand=random.randrange(10)

                if rand==0:

                    self.fall=True

        if not self.fall:

            self.rect.x+=self.xmove

        else:

            self.rect.y+=self.ymove
