#! /usr/bin/env python

import pygame
from pygame.locals import *
from Constants import *
from Walls import *
from Person import *
from Player import *
from Generator import *
from Ladder import *
from Fireball import *
from Donkey import *
from Dumper import *
from Princess import *

if __name__=="__main__":
	
	pygame.init()

	pygame.display.set_caption('Donkey Kong - 2')

	gameExit=False

	clock = pygame.time.Clock()

	FPS=30

	# Creating Player Group

	player_group   =    pygame.sprite.Group()
	player         =    PLAYER(floor_height,bottom-Hero_Height,20,20,'IMG/Hero2.png',screen)
	player_group.add(player)   


	# Creating Kong Group

	kong_group     =    pygame.sprite.Group()

	# Creating Fire Ball Group

	ball_group     =    pygame.sprite.Group()
	ball=[]

	# Creating Fire Ball Dump

	dumper         =    DUMP(floor_height,bottom-Hero_Height,20,20,screen)
	dumper_group   =    pygame.sprite.Group()
	dumper_group.add(dumper)

	queen           =    QUEEN(floor_height+9*block_size,top+floor_height,20,40,'IMG/princess.png',screen)
	queen_group     =    pygame.sprite.Group()
	queen_group.add(queen)

	lives=3
	gap=[]
	kong=[]
	count=[]
	score=0
	iteration=0

	# Game Loop

	while not gameExit:
		kong           +=   [ KONG(40,140,40,40,'IMG/kong2.png',screen) ,]
		kong_group.add(kong[-1])
		count.append(1)
		ball.append(BALL(20,160,20,20,screen))
		ball_group.add(ball)
		gap.append(100)
		level_cleared=False
		coin_group=pygame.sprite.Group()
		for floor in range(1,6):

		    for region in range(3):

		        temp=2*(region+1)+1-floor%2
		        posx=temp*200 -140 + floor_height + (random.randrange(3)*block_size)
		        if posx>=right:
		            posx=right-random.randrange(1,4)*floor_height
		        posy=(random.randrange(3)*block_size + (bottom-floor*diff+floor_height))
		        coin_group.add(COINS(posx,posy,block_size,block_size))
		player.rect.x=floor_height
		player.rect.y=bottom-floor_height
		while not level_cleared and not gameExit and lives>0:
		    for event in pygame.event.get():
			if event.type==QUIT:
			    gameExit=True
			    break
		    collided=False
		    for curball in ball_group:
		        if curball.rect.x<=40 and curball.rect.y==560:
		            continue
			collision=pygame.sprite.spritecollide(curball,player_group,True)
			collided=collided or len(collision)>0
		        if collided:
			    lives-=1
			    player=PLAYER(floor_height,bottom-Hero_Height,20,20,'IMG/Hero2.png',screen)
			    player_group.add(player)
		            break
		    if lives==0:
			gameExit=True
			break

		    inc_x=0
		    inc_y=0

		    keys=pygame.key.get_pressed()
		    if keys[K_q]:
		        gameExit=True
		        break
		    if not player.injump and player.canjump() and keys[K_SPACE]:

			player.injump=True
		    
		    if keys[K_a] or keys[K_LEFT]:
			inc_x=-Hero_xmove

		    elif keys[K_d] or keys[K_RIGHT]:
			inc_x=Hero_xmove

		    if keys[K_w] or keys[K_UP]:
			inc_y=-Hero_ymove

		    elif keys[K_s] or keys[K_DOWN]:
			inc_y=Hero_ymove

		    for i in range(len(gap)):
			gap[i]-=1

		    for i in range(len(count)):
			if count[i]!=10 and gap[i]==0:
			    gap[i]=100
			    count[i]+=1
			    ball.append(BALL(kong[i].rect.x,kong[i].rect.y,20,20,screen))
			    ball_group.add(ball[-1])

		    for curball in ball:
			if curball in ball_group:
			    curball.movement()

		    player.movement(inc_x,inc_y)
		    for i in range(len(kong)):
			kong[i].movement()
		    balls_removed=pygame.sprite.spritecollide(dumper,ball_group,True)
		    if len(balls_removed) > 0:
			ball.append(BALL(kong[0].rect.x,kong[0].rect.y,20,20,screen))
			ball_group.add(ball[-1])
		    screen.fill(black)
		    pygame.draw.rect(screen,red,(5,5,180,50)) 
		    pygame.draw.rect(screen,red,(1145,5,120,50)) 
		    Coins_Collected=pygame.sprite.spritecollide(player,coin_group,True)
		    for coins in coin_group:

			coins.rotate((iteration//4)%6)

		    for fire in ball_group:

			fire.rotate((iteration//3)%4)

		    score+=len(Coins_Collected)*5
		    if(player.rect.left==queen.rect.right and player.rect.bottom==queen.rect.bottom):
		        level_cleared=True
		        
		    screen.blit(pygame.font.SysFont(None,44).render("Score: "+str(score),True,black),(8,15))
		    screen.blit(pygame.font.SysFont(None,44).render("Lives: "+str(lives),True,black),(1148,15))
		    dumper_group.draw(screen)
		    wall_group.draw(screen)
		    ladder_group.draw(screen)
		    queen_group.draw(screen)
		    coin_group.draw(screen)
		    player_group.draw(screen)
		    ball_group.draw(screen)
		    kong_group.draw(screen)
		    pygame.display.update()
		    iteration+=1
		    clock.tick(FPS)

	pygame.quit()
