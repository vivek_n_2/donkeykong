#! /usr/bin/env python

import pygame
from pygame.locals import *
from Constants import *

class PERSON(pygame.sprite.Sprite):
    
    def __init__(self,X,Y,W,H,Image_location,screen):

        super(PERSON,self).__init__()

        self.image=pygame.transform.scale(pygame.image.load(Image_location),(W,H))
        
        self.rect=self.image.get_rect()
        self.rect.x=X
        self.rect.y=Y
        self.image.set_colorkey(white)
