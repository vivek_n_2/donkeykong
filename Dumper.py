#! /usr/bin/env python

import pygame
from pygame.locals import *
from Constants import *
from Generator import *
class DUMP(pygame.sprite.Sprite):

    def __init__(self,X,Y,W,H,screen):

        super(DUMP,self).__init__()
        
        self.image=pygame.Surface([W,H])
        self.image.fill(black)
        self.rect=self.image.get_rect()
        self.rect.x=X
        self.rect.y=Y
