#! /usr/bin/env python

import pygame
from pygame.locals import *
from Constants import *
from Generator import *
from Person import *

class KONG(PERSON):

    def __init__(self,X,Y,W,H,Image_Location,screen):

        super(KONG,self).__init__(X,Y,W,H,Image_Location,screen)
        
        self.xmove=4
        self.ymove=0

    def change(self):

        left   =  False
        right  =  False
        flag   =  False

        for obj in wall_group:
            
            if obj.rect.top==self.rect.bottom:

                if self.rect.left>=obj.rect.left and self.rect.left<obj.rect.right:

                    left = True
                
                if self.rect.right>obj.rect.left and self.rect.right<=obj.rect.right:

                    right = True
            
            if obj.rect.bottom==self.rect.bottom:

                if obj.rect.right==self.rect.left and self.xmove<0:

                    flag=True
                
                if obj.rect.left==self.rect.right and self.xmove>0:

                    flag=True


        for obj in ladder_group:
            
            if obj.rect.top==self.rect.bottom:

                if self.rect.left>=obj.rect.left and self.rect.left<obj.rect.right:

                    left=True
                
                if self.rect.right>obj.rect.left and self.rect.right<=obj.rect.right:

                    right = True

        return not( left and right ) or flag

    def movement(self):
        
        
        if self.change():

            self.xmove*=-1

        self.rect.x+=self.xmove

