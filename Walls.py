#! /usr/bin/env python

import pygame
from pygame.locals import *
from Constants import *

class WALL(pygame.sprite.Sprite):

    def __init__(self,X,Y,W,H,Image_Location,screen):   # (X,Y) is Coordinates of left corner of wall, W is Width , H is Height

        super(WALL,self).__init__()  # Constructor for Parent Class
        
        self.image=pygame.transform.scale(pygame.image.load(Image_Location),(W,H))
        self.rect=self.image.get_rect()
        self.rect.x=X
        self.rect.y=Y
