#! /usr/bin/env python

import pygame
from pygame.locals import *
from Constants import *
from Person import *
from Walls import *
from Ladder import *
from Generator import *

class PLAYER(PERSON):

    def __init__(self,X,Y,W,H,Image_Location,screen):

        super(PLAYER,self).__init__(X,Y,W,H,Image_Location,screen)

        self.injump=False
        self.jumpmove=0

    def canslide(self,inc_y):
        
        for obj in ladder_group:

            if ( self.rect.left>=obj.rect.left and self.rect.left<obj.rect.right ) or \
                    ( self.rect.right>obj.rect.left and self.rect.right<=obj.rect.right ) :
                
                if inc_y>0 and self.rect.bottom>=obj.rect.top and self.rect.bottom<obj.rect.bottom:

                    return True

                if inc_y<0 and self.rect.bottom>obj.rect.top and self.rect.bottom<=obj.rect.bottom:

                    return True

        return False

    def canjump(self):
        
        for obj in ladder_group:

            if obj.rect.top==self.rect.bottom:

                if self.rect.left>=obj.rect.left and self.rect.left<obj.rect.right:

                    #return True
                    return False
                
                if self.rect.right>obj.rect.left and self.rect.right<=obj.rect.right:

                    #return True
                    return False
        
        for obj in wall_group:
            
            if obj.rect.top==self.rect.bottom:

                if self.rect.left>=obj.rect.left and self.rect.left<obj.rect.right:

                    return True
                
                if self.rect.right>obj.rect.left and self.rect.right<=obj.rect.right:

                    return True

        return False

    def isgravity(self):

        if self.injump:

            return False
        
        for obj in wall_group:
            
            if obj.rect.top==self.rect.bottom:

                if self.rect.left>=obj.rect.left and self.rect.left<obj.rect.right:

                    return False
                
                if self.rect.right>obj.rect.left and self.rect.right<=obj.rect.right:

                    return False

        for obj in ladder_group:

            if ( self.rect.left>=obj.rect.left and self.rect.left<obj.rect.right ) or \
                    ( self.rect.right>obj.rect.left and self.rect.right<=obj.rect.right ) :
                
                if self.rect.bottom>=obj.rect.top and self.rect.bottom<=obj.rect.bottom:

                    return False

        return True
            
    def movement(self,inc_x,inc_y):

        if self.injump:

            inc_y=jumps[self.jumpmove]
            self.jumpmove+=1

            if self.jumpmove==len(jumps):

                self.injump=False
                self.jumpmove=0

        elif self.isgravity():
            
            inc_y=Hero_ymove

        elif not self.canslide(inc_y):

            inc_y=0

        self.rect.x+=inc_x
        
        collision_list=pygame.sprite.spritecollide(self,wall_group,False)

        for obj in collision_list:
            if inc_x>0:
                self.rect.right=obj.rect.left

            elif inc_x<0:
                self.rect.left=obj.rect.right

        self.rect.y+=inc_y

        collision_list=pygame.sprite.spritecollide(self,wall_group,False)

        for obj in collision_list:
            
            if inc_y>0:
                self.rect.bottom=obj.rect.top

            elif inc_y<0:
                self.rect.top=obj.rect.bottom
