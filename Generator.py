#! /usr/bin/env python

import pygame
from pygame.locals import *
from Constants import *
from Walls import *
from Coins import *
from Ladder import *
from Images import *
import random

Floor     =    [ floor_height + ( i * block_size ) for i in range( path_len / block_size ) ]
Padding   =    60
Place     =    ( ( path_len - ( 2 * Padding ) ) // 3 ) // block_size
Type      =    [ "GLB" , "GBL" , "LGB" , "LBG" , "BGL" ,"BLG" ]

wall_group     =    pygame.sprite.Group()
coin_group     =    pygame.sprite.Group()
ladder_group   =    pygame.sprite.Group()
broken_group   =    pygame.sprite.Group()

ladder_attributes=[]
broken_attributes=[]
wall_attributes=[]
gap_attributes=[]

for floor in range(1,6):
   
    # yxyxyxyxyxyxy  y is padding in a floor
    # 7y + 6x = path_len
    # y is 60 x is 140

    Areas =    [ ]
    cur   =    Type[[3,5][random.randrange(2)]] if floor==5 else Type[random.randrange(6)]

    for region in range(3):

        temp=2*(region+1)-(floor%2)

        Areas.append( (random.randrange(7)*block_size) + temp*200 - 140)  # 200 is x+y and 140 is x

        if cur[region]=='L':

            ladder_attributes.append( ( Areas[-1],bottom-floor*diff ) )
            Areas.append(Areas[-1]+block_size)
            ladder_attributes.append( ( Areas[-1],bottom-floor*diff ) )

        elif cur[region]=='B':

            broken_attributes.append( ( Areas[-1],bottom-floor*diff , 20 , 25,'IMG/ladder2.png',screen) )
            broken_attributes.append( ( Areas[-1],bottom-floor*diff+55 , 20 , 25,'IMG/ladder3.png',screen) )
            Areas.append(Areas[-1]+block_size)
            broken_attributes.append( ( Areas[-1],bottom-floor*diff , 20 , 25,'IMG/ladder2.png',screen) )
            broken_attributes.append( ( Areas[-1],bottom-floor*diff+55 , 20 , 25,'IMG/ladder3.png',screen) )

        else:

            Areas.append(Areas[-1]+block_size)
            pass

        temp+=1
        
        posx=temp*200 -140 + floor_height + (random.randrange(3)*block_size)

        if posx>=right:

            posx=right-random.randrange(1,4)*floor_height

        posy=(random.randrange(3)*block_size + (bottom-floor*diff+floor_height))

        coin_group.add(COINS(posx,posy,block_size,block_size))

    for X in Floor:
        if X not in Areas:
            wall_attributes.append( ( X,bottom-floor*diff ) )

cage_attributes=[]
for pos in range(8,15):

    if pos!=11:
        cage_attributes.append((Floor[pos],top+3*floor_height))

for pos in range(2):

    cage_attributes.append((Floor[8],top+floor_height+pos*block_size))
    cage_attributes.append((Floor[14],top+floor_height+pos*block_size))

ladder_attributes.append( ( Floor[11],top+3*floor_height) )

wall_attributes+=border_attributes+cage_attributes

for attr in wall_attributes:
    wall_group.add(WALL(attr[0],attr[1],block_size,block_size,'IMG/floor2.jpeg',screen))


for attr in ladder_attributes:
    ladder_group.add(LADDERS(attr[0],attr[1],floor_height,free_space+floor_height,'IMG/ladder.png',screen))

for attr in broken_attributes:
    ladder_group.add(LADDERS(attr[0],attr[1],attr[2],attr[3],attr[4],attr[5]))
