#! /usr/bin/env python

import pygame
from pygame.locals import *
from Constants import *
from Generator import *
from Person import *

class QUEEN(PERSON):

    def __init__(self,X,Y,W,H,Image_Location,screen):

        super(QUEEN,self).__init__(X,Y,W,H,Image_Location,screen)
