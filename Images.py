import pygame
from Constants import *

class SpriteSheet(object):
    
    sprite_sheet = None

    def __init__(self,Location):

        # Load the sprite sheet.
        self.sprite_sheet = pygame.image.load(Location).convert()


    def cut_image(self, x, y, width, height):
        """ Grab a single image out of a larger spritesheet
            Pass in the x, y location of the sprite
            and the width and height of the sprite. """

        # Create a new blank image
        image = pygame.Surface([width, height]).convert()

        # Copy the sprite from the large sheet onto the smaller image
        image.blit(self.sprite_sheet, (0, 0), (x, y, width, height))

        # Assuming black works as the transparent color
        image.set_colorkey(black)

        return image

coin_images=[]
sprite_sheet = SpriteSheet("IMG/coinsheet.png")
for xpos in range(0,463,77):
    image = sprite_sheet.cut_image(xpos, 0, 77, 79)
    coin_images.append(image)

fire_images=[]
sprite_sheet = SpriteSheet("IMG/firesheet2.png")
for xpos in range(0,200,50):
    image = sprite_sheet.cut_image(xpos, 0, 50, 42)
    fire_images.append(image)
#fire_images.append(pygame.transform.flip(image, True, False))
